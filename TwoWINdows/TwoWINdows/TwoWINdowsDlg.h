﻿
// TwoWINdowsDlg.h: файл заголовка
//

#pragma once


// Диалоговое окно CTwoWINdowsDlg
class CTwoWINdowsDlg : public CDialogEx
{
// Создание
public:
	CTwoWINdowsDlg(CWnd* pParent = nullptr);	// стандартный конструктор

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_TWOWINDOWS_DIALOG };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// поддержка DDX/DDV


// Реализация
protected:
	HICON m_hIcon;

	// Созданные функции схемы сообщений
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	CString MESSAGE;
	afx_msg void OnEnChangeMessage();
	HANDLE hevent[3];
	bool work;
	DWORD dwThread;
	HANDLE hThread, hObject;
	PVOID lpBaseAddress;
	void WriteMessage();
	void ReadMessage();
	afx_msg void OnClose();
};
