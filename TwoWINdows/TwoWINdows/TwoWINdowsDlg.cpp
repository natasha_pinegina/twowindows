﻿
// TwoWINdowsDlg.cpp: файл реализации
//

#include "pch.h"
#include "framework.h"
#include "TwoWINdows.h"
#include "TwoWINdowsDlg.h"
#include "afxdialogex.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Диалоговое окно CAboutDlg используется для описания сведений о приложении

class CAboutDlg : public CDialogEx
{
public:
	CAboutDlg();

// Данные диалогового окна
#ifdef AFX_DESIGN_TIME
	enum { IDD = IDD_ABOUTBOX };
#endif

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // поддержка DDX/DDV

// Реализация
protected:
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialogEx(IDD_ABOUTBOX)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialogEx)
END_MESSAGE_MAP()


// Диалоговое окно CTwoWINdowsDlg



CTwoWINdowsDlg::CTwoWINdowsDlg(CWnd* pParent /*=nullptr*/)
	: CDialogEx(IDD_TWOWINDOWS_DIALOG, pParent)
	, MESSAGE(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CTwoWINdowsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_MESSAGE, MESSAGE);
}

BEGIN_MESSAGE_MAP(CTwoWINdowsDlg, CDialogEx)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_EN_CHANGE(IDC_MESSAGE, &CTwoWINdowsDlg::OnEnChangeMessage)
	ON_WM_CLOSE()
END_MESSAGE_MAP()


DWORD WINAPI MyProc(PVOID pv)
{
	CTwoWINdowsDlg* p = (CTwoWINdowsDlg*)pv;
	while (1)
	{
		DWORD dw = WaitForMultipleObjects(3, p->hevent, false, INFINITE);
		switch (dw - WAIT_OBJECT_0)
		{
			case 0:
			{
				ResetEvent(p->hevent[0]);
				break;
			}
			case 1:
			{
				p->SetWindowText(_T("Окно-отправитель"));
				p->work = true;
				p->GetDlgItem(IDC_MESSAGE)->EnableWindow(1);
				return 1;
			}
			case 2:
			{
				p->ReadMessage();
				break;
			}
		}
	}
	return 0;
}

// Обработчики сообщений CTwoWINdowsDlg

BOOL CTwoWINdowsDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// Добавление пункта "О программе..." в системное меню.

	// IDM_ABOUTBOX должен быть в пределах системной команды.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != nullptr)
	{
		BOOL bNameValid;
		CString strAboutMenu;
		bNameValid = strAboutMenu.LoadString(IDS_ABOUTBOX);
		ASSERT(bNameValid);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Задает значок для этого диалогового окна.  Среда делает это автоматически,
	//  если главное окно приложения не является диалоговым
	SetIcon(m_hIcon, TRUE);			// Крупный значок
	SetIcon(m_hIcon, FALSE);		// Мелкий значок

	hevent[0] = CreateEvent(NULL,TRUE,TRUE,_T("o_or_p"));
	hevent[1] = CreateEvent(NULL,FALSE,FALSE,_T("close"));
	hevent[2] = CreateEvent(NULL,TRUE,FALSE,_T("tex"));

	if (WaitForSingleObject(hevent[0], 0) == WAIT_OBJECT_0)
	{
		ResetEvent(hevent[0]);
		SetWindowText(_T("Окно-отправитель"));
		work = true;
	}
	else
	{
		GetDlgItem(IDC_MESSAGE)->EnableWindow(0);
		SetWindowText(_T("Окно-получатель"));
		work = false;
		hThread = CreateThread(NULL,0,MyProc,this,0,&dwThread);
	}
	

	hObject = CreateFileMapping(INVALID_HANDLE_VALUE,NULL,PAGE_READWRITE,0,2048,TEXT("File_mapping"));
	if (hObject != NULL)
	{
		lpBaseAddress = MapViewOfFile(hObject,FILE_MAP_READ | FILE_MAP_WRITE,0,0,0);
	}

	return TRUE;  // возврат значения TRUE, если фокус не передан элементу управления
}

void CTwoWINdowsDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialogEx::OnSysCommand(nID, lParam);
	}
}

// При добавлении кнопки свертывания в диалоговое окно нужно воспользоваться приведенным ниже кодом,
//  чтобы нарисовать значок.  Для приложений MFC, использующих модель документов или представлений,
//  это автоматически выполняется рабочей областью.

void CTwoWINdowsDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // контекст устройства для рисования

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Выравнивание значка по центру клиентского прямоугольника
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Нарисуйте значок
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialogEx::OnPaint();
	}
}

// Система вызывает эту функцию для получения отображения курсора при перемещении
//  свернутого окна.
HCURSOR CTwoWINdowsDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



void CTwoWINdowsDlg::OnEnChangeMessage()
{
	if (work)
	{
		WriteMessage();
		PulseEvent(hevent[2]);
	}
}

void CTwoWINdowsDlg::WriteMessage()
{
	if (lpBaseAddress != NULL) 
		GetDlgItem(IDC_MESSAGE)->GetWindowText((PTSTR)lpBaseAddress,2048);
}
void CTwoWINdowsDlg::ReadMessage()
{
	if (lpBaseAddress != NULL)
		GetDlgItem(IDC_MESSAGE)->SetWindowText((PTSTR)lpBaseAddress);
}

void CTwoWINdowsDlg::OnClose()
{
	if (work)
		SetEvent(hevent[1]);
	else 
	{
		TerminateThread(hThread, 0);
		CloseHandle(hThread);
	}
	UnmapViewOfFile(lpBaseAddress);		// отключение от текущего процесса объекта файлового отображения
	CloseHandle(hObject);
	
	CDialogEx::OnClose();
}
