﻿
// TwoWINdows.h: главный файл заголовка для приложения PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error "включить pch.h до включения этого файла в PCH"
#endif

#include "resource.h"		// основные символы


// CTwoWINdowsApp:
// Сведения о реализации этого класса: TwoWINdows.cpp
//

class CTwoWINdowsApp : public CWinApp
{
public:
	CTwoWINdowsApp();

// Переопределение
public:
	virtual BOOL InitInstance();

// Реализация

	DECLARE_MESSAGE_MAP()
};

extern CTwoWINdowsApp theApp;
